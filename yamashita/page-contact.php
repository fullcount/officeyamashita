<?php
/*
Template Name: お問い合わせ
*/
?>

<?php get_header(); ?>
<div id="pageTitle" class="c-flex--col c-jus-center">
        <div class="l-container">
            <h1>お問い合わせ</h1>
        </div>
    </div>
    <div id="crumb">
        <ul class="l-container">
            <li><a href="../">トップページ</a></li>
            <li>お問い合わせ</li>
        </ul>
    </div>
<div class="l-content">
  <div id="contact">
    <div class="l-container">
      <section id="contact_form">
        <?php if(have_posts()): while(have_posts()):the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile; endif; ?>
      </section>
    </div>
  </div>
</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>​