<?php
/*
Template Name: 新着情報（一覧）
*/
//get_header();
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
  <meta name="viewport" content="width=device-width,user-scalable=no,shrink-to-fit=yes">
  <script src="<?php echo get_template_directory_uri();?>/js/viewport.js"></script>
  <meta name="robots" content="all">
  <meta content="True" name="HandheldFriendly">
  <meta name="format-detection" content="telephone=no">
  <meta name="description" content="<?php echo $s_desc?>">
  <meta name="Keywords" content="<?php echo $s_key?>">
  <title>
    <?php echo $s_title?>
  </title>
  <?php wp_head(); ?>
</head>

<body>
  <div id="wrapper" class="page_company">

    <?php get_header();?>
    <div id="pageTitle" class="c-flex--col c-jus-center">
      <div class="l-container">
        <h1>お知らせ</h1>
      </div>
    </div>
    <div id="crumb">
      <ul class="l-container">
        <li><a href="<?php echo home_url();?>/">トップページ</a>
        </li>
        <li>お知らせ一覧</li>
      </ul>
    </div>
    <div class="l-content">
      <div class="l-container">
          <div class="p-news c-block">
            <?php
            $args = array(
              'posts_per_page' => 20 // 表示件数の指定
            );
            $posts = get_posts( $args );
            foreach ( $posts as $post ): // ループの開始
              setup_postdata( $post ); // 記事データの取得
            ?>
            <dl class="p-news__block">
              <dt class="p-news__date">
                <?php the_time('Y/m/d'); ?>
              </dt>
              <dd class="p-news__ttl">
                <a href="<?php the_permalink(); ?>">
                  <?php the_title(); ?>
                </a>
              </dd>
              <?php
              endforeach; // ループの終了
              wp_reset_postdata(); // 直前のクエリを復元する
              ?>
            </dl>
            <!--<div class="p-news__button"><a href="./news/" class="c-button c-button__df c-button__arrow c-button__arrow-right">詳しくはこちら</a></div>-->
          </div>
      </div>
    </div>
    <?php get_footer();?>

  </div>
  <!--/site-wrap-->

  <!-- scripts -->
  <?php wp_footer(); ?>
</body>
</html>
<?php
//get_footer();
?>