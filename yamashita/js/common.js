$(function(){
    var ua = navigator.userAgent;
    if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
        $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
    } else {
        $('head').prepend('<meta name="viewport" content="width=1000">');
    }
	
    var timer = false;
    var prewidth = $(window).width()
    $(window).resize(function() {
        if (timer !== false) {
            clearTimeout(timer);
        };
        timer = setTimeout(function() {
            var nowWidth = $(window).width()
            if(prewidth !== nowWidth){
                location.reload();
            };
            prewidth = nowWidth;
        }, 200);
    });
});


//----------------------------------------------------------------
//----------------------------------------------------------------
//スムーズスクロール
//----------------------------------------------------------------
//----------------------------------------------------------------


//gotoTOP

$(function() {
    var showFlag = false;
    var topBtn = $('.page-top');    
     topBtn.css('bottom', '-150px');
    var showFlag = false;
    //スクロールが100に達したらボタン表示
$(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            if (showFlag == false) {
                showFlag = true;
                topBtn.stop().animate({'bottom' : '20px'}, 200);
            }
        } else {
            if (showFlag) {
                showFlag = false;
                topBtn.stop().animate({'bottom' : '-150px'}, 200);
            }
        }
    });
    //スクロールしてトップ
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});
$(function(){
  $('a[href^="#"]').click(function(){
    var speed = 500;
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $("html, body").animate({scrollTop:position}, speed, "swing");
    return false;
  });
});



//----------------------------------------------------------------
//----------------------------------------------------------------
//サブメニュー
//----------------------------------------------------------------
//----------------------------------------------------------------

/*
$(function() {
	var nav = $('#gnav');
	var navTop = nav.offset().top;
	$('.subnav').hide();
	if(!navigator.userAgent.match(/(iPhone|iPad|iPod|Android)/)){
	$('li', nav).hover(function(){
		$('.subnav',this).stop().slideDown('fast');
	},function(){
		$('.subnav',this).stop().slideUp('fast');
	});} else {
		$('li', nav).click(function(){
			$('.subnav',this).slideToggle('fast');
		});
	}
});

*/

//----------------------------------------------------------------
//----------------------------------------------------------------
// SPハンバーガー
//----------------------------------------------------------------
//----------------------------------------------------------------

$(function() {
    var menu = $('.gnav'), // スライドインするメニューを指定
        menuBtn = $('#nav-toggle'), // メニューボタンを指定
        body = $(document.body),
        menuWidth = menu.outerWidth(),
        state = false,
        scrollpos;
    // メニューボタンをクリックした時の動き
    menuBtn.on('click', function() {
        // body に open クラスを付与する
        body.toggleClass('open');
        if (body.hasClass('open')) {
            // open クラスが body についていたらメニューをスライドインする
            body.animate({
                'right': menuWidth
            }, 300);
            menu.animate({
                'right': 0
            }, 300);
        } else {
            // open クラスが body についていなかったらスライドアウトする
            menu.animate({
                'right': -menuWidth
            }, 300);
            body.animate({
                'right': 0
            }, 300);
        }
      //ページ内アンカー用
      $('#spnav a[href]:not(.tel-link a)').click(function(){
        body.removeClass('open');
        body.removeClass('bg_fixed');
        menu.animate({
          'right': -menuWidth
        }, 300);
        body.animate({
          'right': 0
        }, 300);
      });
      //背景固定
      if(state == false) {
        scrollpos = $(window).scrollTop();
        body.addClass('bg_fixed').css({'top': -scrollpos});
        $('.spnav').addClass('open');
        state = true;
      } else {
        body.removeClass('bg_fixed').css({'top': 0});
        window.scrollTo( 0 , scrollpos );
        $('.spnav').removeClass('open');
        state = false;
      }
    });
});






//----------------------------------------------------------------
//----------------------------------------------------------------
//プルダウン
//----------------------------------------------------------------
//----------------------------------------------------------------



$(function() {
    function spnav() {
        $(this).toggleClass("active").next().slideToggle(300);
    }
    $(".switch .toggle").click(spnav);

});
//----------------------------------------------------------------
//----------------------------------------------------------------
//タブ
//----------------------------------------------------------------
//----------------------------------------------------------------

$(function(){
	$('.tabbox:first').show();
	$('.tab li:first').addClass('active');
	$('.tab li').click(function() {
		$('.tab li').removeClass('active');
		$(this).addClass('active');
		$('.tabbox').hide();
		$($(this).find('a').attr('href')).fadeIn();
		return false;
	});
});


//----------------------------------------------------------------
//----------------------------------------------------------------
//プレースホルダー
//----------------------------------------------------------------
//----------------------------------------------------------------



$(function() {
    // placeholder（IE9対応）
    if ($.fn.placeholder) {
        $('input, textarea').placeholder();
    }

});

//背景固定
$(function(){
  var state = false;
  var scrollpos;
 
  $('#nav-toggle').on('click', function(){
    if(state == false) {
      scrollpos = $(window).scrollTop();
      $('body').addClass('bg_fixed').css({'top': -scrollpos});
      $('.spnav').addClass('open');
      state = true;
    } else {
      $('body').removeClass('bg_fixed').css({'top': 0});
      window.scrollTo( 0 , scrollpos );
      $('.spnav').removeClass('open');
      state = false;
    }
  });
 
});


/*
$(function() {
    $(".move").hide();
    $(".show li").hover(function() {

        $(".move", this).stop(true, true).slideDown();
    },

    function() {
        $(".move", this).stop(true, true).slideUp();
    });
});
*/

//----------------------------------------------------------------

//同意チェック
//----------------------------------------------------------------
$(function() {
  $('#check').prop('checked', false);

  $('#check').on('click', function() {
    if ( $(this).prop('checked') == false ) {
      $('#submit').prop('disabled', true);
    } else {
      $('#submit').prop('disabled', false);
    }
  });
});




//----------------------------------------------------------------
//----------------------------------------------------------------
//スマホTEL
//----------------------------------------------------------------
//----------------------------------------------------------------

$(function(){
    var ua = navigator.userAgent;
    if(ua.indexOf('iPhone') > 0 && ua.indexOf('iPod') == -1 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0 ){
        $('.js-tel').each(function(){
            var str = $(this).text();
            $(this).html($('<a>').attr('href', 'tel:' + str.replace(/-/g, '')).append(str + '</a>'));
        });
    }
});
$(function(){
    var ua = navigator.userAgent;
    if(ua.indexOf('iPhone') > 0 && ua.indexOf('iPod') == -1 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0 && ua.indexOf('SC-01C') == -1 && ua.indexOf('A1_07') == -1 ){
        $('.tel-link-img img').each(function(){
            var alt = $(this).attr('alt');
            $(this).wrap($('<a>').attr('href', 'tel:' + alt.replace(/-/g, '')));
        });
    }
});





//----------------------------------------------------------------
//----------------------------------------------------------------
//サムネイルクリックで拡大
//----------------------------------------------------------------
//----------------------------------------------------------------
$(window).on('load resize', function() {
    //写真の高さ取得
    var photoH = $(".photo figure > img").innerHeight();
    $(".photo figure").css("height", photoH + "px");
});
$(function() {
    var click_flg = false;
    $(".photo .thumbnail a").click(function() {
        if (click_flg == false) {
            click_flg = true;
            $(".photo figure img").before("<img class='demo' src='" + $(this).attr("href") + "' alt=''>");
            $(".photo figure img:last").stop(true, false).fadeOut("fast", function() {
                $(this).remove();
                click_flg = false;
            });
            return false;
        } else {
            return false;
        }
    });
});
//----------------------------------------------------------------
//----------------------------------------------------------------
//アコーディオン
//----------------------------------------------------------------
//----------------------------------------------------------------


//ACCORDION
$(function(){
    $(".accordionbox dt").on("click", function() {
        $(this).next().slideToggle();   
        // activeが存在する場合
        if ($(this).children("span").hasClass('active')) {           
            // activeを削除
            $(this).children("span").removeClass('active');              
        }
        else {
            // activeを追加
            $(this).children("span").addClass('active');         
        }           
    });
});
