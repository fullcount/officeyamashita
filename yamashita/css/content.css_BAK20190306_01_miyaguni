@charset "UTF-8";

/*レイアウト
*********************************************/

.l-container {}

@media screen and (max-width: 736px) {
  .l-container {
    padding: 0 15px;
  }
}
#underconstruction {
  padding: 50px 0 200px;
  text-align: center;
}


/*pagetitle
*********************************************/

#pageTitle {
  width: 100%;
  height: 150px;
  background: url(../images/pageTitle_bg.png);
  background-size: cover;
  background-position: center center;
}

#pageTitle h1 {
  text-align: center;
  background-color: rgba(62, 138, 223, 0.7);
  font-size: 2.8rem;
  color: #FFF;
  line-height: 50px;
  font-weight: normal;
  width: 1000px;
}

@media screen and (max-width: 736px) {
  #pageTitle {
    height: 30vw;
  }
  #pageTitle h1 {
    font-size: 2.2rem;
    width: 100%;
  }
}


/*crumb
*********************************************/

#crumb {
  /*background-color: #004b80;*/
}

#crumb ul {
  overflow: hidden;
}

#crumb li {
  float: left;
  margin-right: 17px;
  font-size: 1.2rem;
  position: relative;
  line-height: 24px;
}

#crumb li:before {
  content: ">";
  position: absolute;
  right: -13px;
  top: 0px;
}

#crumb li:last-child:before {
  display: none;
}

#crumb a {
  text-decoration: none;
}

#crumb a:hover {
  text-decoration: underline;
}

.crumb ul .home a {
  padding-left: 1.5em;
  background-size: 25%;
}

@media screen and (max-width: 736px) {
  #crumb {
    display: none;
  }
}


/*見出し
*********************************************/

.sec_ttl {
  position: relative;
  margin-bottom: 50px;
}

.sec_ttl::after {
  position: absolute;
  display: block;
  content: '';
  background: url(../images/sec_ttl_border.png) no-repeat;
  background-position: center center;
  background-size: contain;
  width: 1000px;
  height: 10px;
  bottom: 0;
}

.sec_ttl h2 {
  font-size: 3.0rem;
  padding-bottom: 20px;
  text-align: center;
}

.sec_ttl h2:first-letter {
  font-size: 3.6rem;
  color: #3e8adf;
}

.block_ttl h3 {
  font-size: 1.8rem;
  font-weight: bold;
  color: #3e8adf;
  line-height: 1.2;
}

@media screen and (max-width: 736px) {
  .sec_ttl {
    position: relative;
    margin-bottom: 20px;
  }
  .sec_ttl::after {
    width: 100%;
    height: 3vw;
  }
  .sec_ttl h2 {
    font-size: 1.8rem;
  }
  .sec_ttl h2:first-letter {
    font-size: 2.2rem;
  }
}


/*office
*********************************************/

#office section {
  padding: 50px 0;
}

#office #plofile .inner{
  padding: 20px;
  background: url(../images/office/bg_profile.png) no-repeat;
  background-size: cover;
  background-position: top center;
}

#office #plofile .txt {
  width: 60%;
}

#office #plofile .photo {
  width: 40%;
  text-align: center;
}

#office #plofile .block+.block {
  margin-top: 30px
}

#office #plofile .comment {
  margin-top: 1em;
  line-height: 1.5;
}

#office #plofile .comment p+p {
  margin-top: 1em;
}

#office #gaiyou table {
  width: 100%;
}

#office #gaiyou table tr th, #office #gaiyou table tr td {
  padding: 15px;
  text-align: left;
  line-height: 1.5;
}

#office #gaiyou table tr th {
  border-bottom: 3px solid #3e8adf;
}

#office #gaiyou table tr td {
  border-bottom: 3px solid #F5F5F5;
}

@media screen and (max-width: 736px) {
  #office section {
    padding: 30px 0;
  }
  #office #plofile .txt {
    width: 100%;
  }
  #office #plofile .photo {
    width: 100%;
    margin-bottom: 15px;
  }
  #office #gaiyou table tr th, #office #gaiyou table tr td {
    width: 100%;
    display: block;
  }
  #office #gaiyou table tr th {
    padding: 5px 15px;
    background: #3e8adf;
    color: #FFF;
    text-align: center;
  }
  #office #gaiyou table tr td {
    border-bottom: 0;
  }
}

/*price
*********************************************/

#price section {
  padding: 50px 0;
}

#price .anchor ul li {
  width: 16%;
}

#price .anchor ul li a {
  display: flex;
  -webkit-align-items: center;
  align-items: center;
  -webkit-justify-content: center;
  justify-content: center;
  width: 100%;
  height: 50px;
  text-align: center;
  text-decoration: none;
  border-radius: 5px;
  line-height: 1.4;
  box-sizing: border-box;
}

#price .anchor ul li:nth-child(1) a {
  color: #ee8585;
  border: 1px dotted #ee8585;
}

#price .anchor ul li:nth-child(2) a {
  color: #928ccf;
  border: 1px dotted #928ccf;
}

#price .anchor ul li:nth-child(3) a {
  color: #32d3a0;
  border: 1px dotted #32d3a0;
}

#price .anchor ul li:nth-child(4) a {
  color: #3e8adf;
  border: 1px dotted #3e8adf;
}

#price .anchor ul li:nth-child(5) a {
  color: #39aab1;
  border: 1px dotted #39aab1;
}

#price .anchor ul li:nth-child(6) a {
  color: #666;
  border: 1px dotted #666;
}

#price .outer {
  padding: 20px;
}

#price #anc01 .outer {
  padding: 20px;
  background: rgba(238, 133, 133, 0.7);
}

#price #anc02 .outer {
  padding: 20px;
  background: rgba(146, 140, 207, 0.7);
}

#price #anc03 .outer {
  padding: 20px;
  background: rgba(50, 211, 160, 0.7);
}

#price #anc04 .outer {
  padding: 20px;
  background: rgba(62, 138, 223, 0.7);
}

#price #anc05 .outer {
  padding: 20px;
  background: rgba(57, 170, 177, 0.7);
}

#price #anc06 .outer {
  padding: 20px;
  background: rgba(102, 102, 102, 0.7);
}

#price .inner {
  padding: 40px;
  background: #FFF;
}

#price .inner .btn a {
  line-height: 50px;
  border-radius: 10px;
  color: #FFF;
  text-align: center;
  font-weight: bold;
  margin: 20px auto;
  width: 50%;
}
#price #anc01 .inner .btn a {
  background: #ee8585;
}
#price #anc02 .inner .btn a {
  background: #928ccf;
}
#price #anc03 .inner .btn a {
  background: #32d3a0;
}
#price #anc04 .inner .btn a {
  background: #3e8adf;
}
#price #anc05 .inner .btn a {
  background: #39aab1;
}
#price .p-service__ttl {
  font-size: 2.4rem;
}

#price .inner table {
  width: 100%;
}

#price .inner table tr th, #price .inner table tr td {
  padding: 20px 15px 20px 15px;
  width: 50%;
  font-size: 1.8rem;
}

#price .inner table.anc01 tr th, #price .inner table.anc01 tr td {
  border-top: 1px solid rgba(238, 133, 133, 0.7);
}

#price .inner table.anc02 tr th, #price .inner table.anc02 tr td {
  border-top: 1px solid rgba(146, 140, 207, 0.7);
}

#price .inner table.anc03 tr th, #price .inner table.anc03 tr td {
  border-top: 1px solid rgba(50, 211, 160, 0.7);
}

#price .inner table.anc04 tr th, #price .inner table.anc04 tr td {
  border-top: 1px solid rgba(62, 138, 223, 0.7);
}

#price .inner table.anc05 tr th, #price .inner table.anc05 tr td {
  border-top: 1px solid rgba(57, 170, 177, 0.7);
}

#price .inner table.anc06 tr th, #price .inner table.anc06 tr td {
  border-top: 1px solid rgba(102, 102, 102, 0.5);
}

#price .inner table tr th {
  text-align: left;
  font-weight: bold;
}

#price .inner table tr td {
  text-align: right;
}

#price .inner dl dt, #price .inner dl dd {
  width: 50%;
  text-align: right;
  padding: 10px 5px 10px 5px;
  border-bottom: 1px dotted #CCC;
}

#price #member figure {
  width: 60px;
}

@media screen and (max-width: 736px) {
  #price .p-service__ttl {
    font-size: 1.8rem;
  }
  #price section {
    padding: 30px 0;
  }
  #price .anchor ul li {
    width: 49%;
    margin-bottom: 15px;
  }
  #price .inner {
    padding: 40px 20px 20px;
  }
  #price .inner .btn a {
    width: 100%;
    font-size: 3.3vw;
  }
  #price .inner table tr th, #price .inner table tr td {
    width: 100%;
    display: block;
    text-align: center;
  }
  #price .inner table tr th {
    padding: 5px;
    line-height: 1.6;
  }
}


/*contact
*********************************************/
#contact section {
  padding: 50px 0;
}
#contact .lead {
  margin-bottom: 30px;
}
#contact_form {
  margin: 20px auto;
}

#contact_form h3 {
  padding: 10px;
  background: #cba447;
  color: #fff;
  font-size: 16px
}

#contact_form table {
  width: 100%;
  margin: 0 auto;
  border-collapse: collapse;
  border-bottom: 1px solid #ccc;
  background-color: #fff;
}

#contact_form th {
  width: 300px;
  padding: 20px;
  font-weight: normal;
  text-align: left;
  vertical-align: top;
  border: 1px solid #ccc;
  line-height: 1.5;
  font-size: 1.6rem;
  background-color: #f3f3f3;
}

#contact_form th div {
  text-indent: -12px
}

#contact_form td {
  padding: 20px;
  text-align: left;
  vertical-align: top;
  border: 1px solid #ccc;
  line-height: 1.5;
  color: #666;
}

#contact_form table li {
  display: inline-block;
  list-style: none;
  margin: 5px 0;
  padding: 0 20px 0 0;
  vertical-align: top;
}

#contact_form .fs10 {
  font-size: 10px
}

#contact_form .hissu {
  float: left;
  margin: 0 20px 0 0;
  padding: 2px 5px;
  background: #ee8585;
  color: #fff;
  font-size: 15px;
  width: 50px;
  text-align: center;
  border-radius: 3px;
}

#contact_form .ninni {
  float: left;
  margin: 0 20px 0 0;
  padding: 2px 5px;
  background: #999;
  color: #fff;
  font-size: 15px;
  width: 50px;
  text-align: center;
  border-radius: 3px;
}

input, select, textarea {
  margin-bottom: 1px;
  font-size: 1.3rem;
}

textarea:placeholder-shown {
  font-size: 13px;
  font-size: 1.3rem;
  color: #666;
}


/* Google Chrome, Safari, Opera 15+, Android, iOS */

textarea::-webkit-input-placeholder {
  font-size: 1.3rem;
}


/* Firefox 18- */

textarea:-moz-placeholder {
  font-size: 1.3rem;
}


/* Firefox 19+ */

textarea::-moz-placeholder {
  font-size: 1.3rem;
}


/* IE 10+ */

textarea:-ms-input-placeholder {
  font-size: 1.3rem;
}

optgroup {
  background-color: #ccc;
  color: #333;
  font-style: normal
}

option {
  margin: 0 5px 0 0;
  background-color: #fff;
  color: #333
}

.ime_on {
  ime-mode: active
}

.ime_off {
  ime-mode: disabled
}

.input_txt, .select, .textarea {
  padding: 10px;
  border: none;
  background: #f7f7f8;
  border: 1px solid #efebe5;
  box-sizing: border-box;
}

.textarea {
  resize: none;
  height: 200px;
}

input:focus {
  background: #fff;
}

.form_submit {
  margin: 10px 10px 0;
  padding: 5px;
  text-align: center;
}

.form_submit p:first-child {
  margin-bottom: 15px;
}

.form_submit .comment {
  margin-bottom: 30px;
  text-align: left;
}

.form_submit label {
  padding: 2px 0 0;
  font-weight: normal
}

#contact_form input[type="submit"], #contact_form input[type="button"] {
  width: 300px;
  height: 50px;
  margin-top: 30px;
  background-color: #3e8adf;
  border: none;
  color: transparent;
  -webkit-transition: background-color .2s;
  transition: background-color .2s;
  cursor: pointer;
  -webkit-appearance: none;
  border-radius: 0;
  height: 60px;
  color: #fff;
  font-size: 1.6rem;
  border-radius: 5px;
}

#contact_form input[type="submit"]:hover, #contact_form input[type="button"]:hover {
  background-color: #ee8585;
}

@media screen and (max-width:736px) {
  
  #contact section {
  padding: 0;
}
  #contact_form th {
    display: block;
    padding: 5px 10px;
    border-bottom: none;
    width: inherit;
  }
  #contact_form td {
    display: block;
    padding: 10px 5px;
    border-bottom: none;
  }
  #contact_form td #zip.w15per {
    width: 35%;
  }
  #contact_form td span {
    display: block
  }
  #contact_form .hissu, #contact_form .hissu.en, #contact_form .ninni {
    padding: 0;
  }
  .input_txt, .select {
    background: #fff;
    border: 1px solid #ddd;
  }
  .textarea {
    height: 31.25vw;
  }
}


/*news
*********************************************/
#news {
  padding: 50px 0;
  line-height: 1.5;
  overflow: hidden;
}
#news h1 {
  font-size: 2.2rem;
}
#news h2 {
  font-size: 2.0rem;
}
#news h3 {
  font-size: 1.8rem;
}
#news h4 {
  font-size: 1.6rem;
}
#news h5 {
  font-size: 1.4rem;
}
#news h6 {
  font-size: 1.2rem;
}
#news time {
  padding: 3px 5px;
  background: #8bbdf6;
  color: #FFF;
  border-radius: 5px;
  font-size: 1.2rem;
}
#news .news_btn {
  margin-top: 30px;
}
#news .news_btn .btn a {
  display: block;
  line-height: 40px;
  background: #8bbdf6;
  color: #FFF;
  border-radius: 5px;
  width: 100px
}
@media screen and (max-width:736px) {
  #news {
  padding: 25px 0;
}
}
#news.news_dt h1 {
  background: #F5F5F5;
  padding: 10px;
  margin: 15px 0 20px 0;
  border-top: 1px dotted #8bbdf6;
  border-bottom: 1px dotted #8bbdf6;
}


