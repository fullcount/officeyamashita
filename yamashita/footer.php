<div class="page-top"> <a href="#wrapper"><img src="<?php echo get_template_directory_uri(); ?>/images/gotop.png" alt="PAGE TOP"/></a></div>
        <footer class="l-footer">
            <div class="l-footer__bnr l-container c-flex--between">
                <div class="c-contcard p-contcard__tel">
                    <div class="c-contcard__tel c-flex--alignCenter c-jus-center">
                        <div class="c-contcard__icon"><img src="<?php echo get_template_directory_uri(); ?>/images/ico_tel.png" alt="電話"></div>
                        <p class="c-contcard__number"><span class="js-tel">090-9301-3749</span></p>
                    </div>
                    <p class="c-contcard__time">お電話での受付時間：<span>10:00-18:00</span></p>
                    <p class="c-contcard__comment">土日でのご相談もお気軽にお尋ねください。</p>
                </div>
                <div class="c-contcard p-contcard__mail">
                    <div class="c-contcard__mail c-flex--alignCenter c-jus-center">
                        <div class="c-contcard__icon"><img src="<?php echo get_template_directory_uri(); ?>/images/ico_mail.png" alt="メール"></div>
                        <p class="c-contcard__address">officeyamashita@sk-omotesando.com</p>
                    </div>
                    <p class="c-card__btn p-contcard__mail--button"><a href="<?php echo home_url('/contact'); ?>" class="c-button">お問い合わせフォームはこちら</a></p>
                    <p class="c-contcard__comment">時間外でのご相談もメールでお気軽にお尋ねください。</p>
                </div>
            </div>
            <nav class="fnav l-container">
                <ul class="c-flex--alignCenter c-jus-center">
                    <li class="fnav__item"><a href="<?php echo home_url(); ?>">HOME</a></li>
                    <li class="fnav__item"><a href="<?php echo home_url('/service'); ?>">業務内容</a></li>
                    <li class="fnav__item"><a href="<?php echo home_url('/office'); ?>">事務所案内</a></li>
                    <li class="fnav__item"><a href="<?php echo home_url('/price'); ?>">報酬案内</a></li>
                    <!--<li class="fnav__item"><a href="<?php echo home_url('/news'); ?>">お知らせ</a></li>-->
                    <li class="fnav__item"><a href="<?php echo home_url('/contact'); ?>">お問い合わせ</a></li>
                </ul>
            </nav>
            <div class="l-footer__logo"><img src="<?php echo get_template_directory_uri(); ?>/images/f_logo.png" alt="ベリテ社労士・行政書士事務所"></div>
            <div class="l-footer__address">
                <p>〒107-0062 東京都港区南青山4-20-20　<br class="br-sp">マックス南青山B1<br>
                    表参道駅から徒歩5分　<a href="<?php echo home_url('/office/#map'); ?>">Mapはこちら</a></p>
            </div>
            <?php if(is_front_page()) : ?>
            <div class="l-footer__srp">
            <div class="l-footer__srp__img">
              <img src="<?php echo get_template_directory_uri(); ?>/images/srp.png" alt="SRPⅡ認証事務所">
            </div>
            <div class="l-footer__srp__txt">
            <p>ベリテ社労士事務所はSRPⅡ認証事務所です。<br>特定個人情報の適正な取扱いを実践してまいります。</p>
            <p><a href="https://www.shakaihokenroumushi.jp/organization/tabid/507/Default.aspx" target="_blank" rel="noopener noreferrer">SRPⅡ認証制度とは</a></p></div>
          </div>
          <?php endif; ?>
        </footer>
    </div>
</body>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
    window.jQuery || document.write('<script src="./js/jquery-1.11.2.min.js"><\/script>')
</script>

</html>
<?php wp_footer(); ?>
</body>
</html>
