<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
    <div id="pageTitle" class="c-flex--col c-jus-center">
        <div class="l-container">
            <h1>404 Page not found</h1>
        </div>
    </div>
    <div id="crumb">
        <ul class="l-container">
            <li><a href="../">トップページ</a></li>
            <li>404 Page not found</li>
        </ul>
    </div>
    <div class="l-content">
        <div id="underconstruction">
            <p>申し訳ございません。当サイト内にお探しのページは見つかりませんでした。</p>
            <p>入力したURLが誤っているか、ページが削除されております。</p>
          <p style="margin-top: 30px;"><a href="<?php echo home_url(); ?>">トップページへ戻る</a></p>
        </div>
    </div>
<?php get_footer(); ?>
