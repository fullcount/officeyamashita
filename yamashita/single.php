<?php get_header(); ?>
<div id="pageTitle" class="c-flex--col c-jus-center">
  <div class="l-container">
    <h1>お知らせ</h1>
  </div>
</div>
<div id="crumb">
  <ul class="l-container">
    <li><a href="<?php echo home_url(); ?>">トップページ</a></li>
    <!--<li><a href="<?php echo home_url('news'); ?>">お知らせ一覧</a></li>-->
    <li><?php the_title(); ?></li>
  </ul>
</div>
<div class="l-content">
  <div class="l-container">
    <div id="news" class="news_dt">
      <?php if(have_posts()): while(have_posts()):the_post(); ?>
      <time datetime="<?php the_time('Y-m-d'); ?>">
        <?php the_time('Y.m.d'); ?>
      </time>
      <h1><?php the_title(); ?></h1>
      <!--<p>
        <?php the_category(', '); ?>
      </p>-->
      <div class="news-content">
        <?php the_content('Read more'); ?>
      </div>
      <?php endwhile; endif; ?>

      <div class="news_btn c-flex--between">
      <div class="btn"><?php previous_post_link('%link','前へ'); ?></div>
      <!--<div class="btn"><a href="<?php echo home_url('news'); ?>">一覧へもどる</a></div>-->
      <div class="btn"><?php next_post_link('%link','次へ'); ?></div>
        </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>