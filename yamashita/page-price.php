<?php
/*
Template Name: 報酬案内
*/
get_header();
?>
    <div id="pageTitle" class="c-flex--col c-jus-center">
        <div class="l-container">
            <h1>報酬案内</h1>
        </div>
    </div>
    <div id="crumb">
        <ul class="l-container">
            <li><a href="../">トップページ</a></li>
            <li>報酬案内</li>
        </ul>
    </div>
    <div class="l-content">
        <div id="price">
            <section id="lead">
                <div class="l-container">
                    <div class="comment">
                        <p>※消費税10%込みの報酬額です。<br>
                          ※印紙、証紙等の実費は、別途申し受けます。<br>
                          ※お客様の状況により、価格は前後する場合がございます。まずはご依頼内容をお伺いし、お見積もりさせていただきます。お見積もりは無料です。</p>
                    </div>
                </div>
            </section>
            
            <div class="bg01">
            <div class="l-container">
              <div class="sec_ttl">
                <h2>社会保険労務士業務</h2>
              </div>
            </div>
            <section id="anc05">
                <div class="l-container">
                    <div class="outer">
                        <div class="inner">
                            <table class="anc05">
                                <tr>
                                    <th>顧問契約（月額）
                                    <ul>
                                      <li>上記に含まれる業務内容</li>
                                      <li>・労働保険・社会保険の手続き代行、提出</li>
                                      <li>・法定帳簿書類<br class="br-sp">（労働者名簿・賃金台帳・出勤簿）の作成</li>
                                      <li>・人事労務関係の相談・指導</li>
                                    </ul>
                                    <ul class="u-mt-m">
                                      <li>下記は別途費用となります</li>
                                      <li>労働基準法／就業規則・寄宿舎規則等の作成／労災・社会保険法／長期休業・障害・死亡給付手続き成／雇用保険／各種助成金等の給付申請成／労働安全衛生法／許認可申請等</li>
                                    </ul>
                                    </th>
                                    <td><dl class="c-flex--between">
                                            <dt>従業員数4名以下</dt>
                                            <dd>22,000円</dd>
                                            <dt>10名まで</dt>
                                            <dd>33,000円</dd>
                                            <dt>20名まで</dt>
                                            <dd>44,000円</dd>
                                            <dt>30名まで</dt>
                                            <dd>55,000円</dd>
                                            <dt>50名まで</dt>
                                            <dd>66,000円</dd>
                                            <dt>100名まで</dt>
                                            <dd>88,000円</dd>
                                            <dt>200名まで</dt>
                                            <dd>154,000円</dd>
                                        </dl>
                                        <span>※従業員数には<br class="br-sp">パート、アルバイトも含みます。</span></td>
                                </tr>
                                <tr>
                                    <th>派遣業許可申請</th>
                                    <td>165,000円～</td>
                                </tr>
                                <tr>
                                    <th>派遣業許可更新申請</th>
                                    <td>110,000円～</td>
                                </tr>
                                <tr>
                                    <th>派遣業各種報告書作成</th>
                                    <td>33,000円～</td>
                                </tr>
                                <tr>
                                    <th>就業規則作成</th>
                                    <td>165,000円～</td>
                                </tr>
                                <tr>
                                    <th>その他規定作成</th>
                                    <td>55,000円～</td>
                                </tr>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </section>
            </div>
            
            <div class="bg02">
            <div class="l-container">
              <div class="sec_ttl">
                <h2>行政書士業務</h2>
              </div>
            </div>
            <section id="anc05">
                <div class="l-container">
                    <div class="outer">
                        <div class="inner">
                            <p class="p-service__ttl u-c-darkgreen">知的資産・著作権関係</p>
                            <table class="anc05">
                                <tr>
                                    <th>著作権登録申請<br class="br-sp">（プログラム関係を除く）
                                    <ul>
                                      <li>実名の登録</li>
                                      <li>第一発行年月日等の登録</li>
                                      <li>著作権・著作隣接権の移転等の登録</li>
                                      <li>出版権の設定等の登録</li>
                                      <li>著作権者不明等の場合の裁定申請</li>
                                    </ul></th>
                                    <td>55,000円～</td>
                                </tr>
                                <tr>
                                    <th>プログラムの著作物に係る<br class="br-sp">登録申請
                                    <ul>
                                      <li>創作年月日の登録</li>
                                      <li>実名の登録</li>
                                      <li>第一発行年月日等の登録</li>
                                      <li>著作権移転等の登録</li>
                                    </ul></th>
                                    <td>110,000円～</td>
                                </tr>
                                <tr>
                                    <th>知的資産経営報告書作成</th>
                                    <td>220,000円～</td>
                                </tr>
                                <tr>
                                    <th>小規模事業者持続化補助金</th>
                                    <td>33,000円＋成果報酬10％</td>
                                </tr>
                                <tr>
                                    <th>報告書作成</th>
                                    <td>33,000円</td>
                                </tr>
                            </table>
                            <!--<div class="btn"><a href="./service/copyright">知的資産・著作権関係のサービス内容を見る</a></div>-->
                        </div>
                    </div>
                </div>
            </section>
            <section id="anc05">
                <div class="l-container">
                    <div class="outer">
                        <div class="inner">
                            <p class="p-service__ttl u-c-darkgreen">入管・国際法務関係</p>
                            <table class="anc05">
                                <tr>
                                    <th>在留資格認定証明書交付申請</th>
                                    <td><dl class="c-flex--between">
                                            <dt>経営・管理</dt>
                                            <dd>198,000円</dd>
                                            <dt>就労資格</dt>
                                            <dd>132,000円</dd>
                                            <dt>身分関係</dt>
                                            <dd>110,000円</dd>
                                        </dl></td>
                                </tr>
                                <tr>
                                    <th>在留期間更新許可申請</th>
                                    <td><dl class="c-flex--between">
                                            <dt>経営・管理</dt>
                                            <dd>88,000円</dd>
                                            <dt>その他</dt>
                                            <dd>55,000円</dd>
                                        </dl></td>
                                </tr>
                                <tr>
                                    <th>在留資格変更許可申請</th>
                                    <td><dl class="c-flex--between">
                                            <dt>経営・管理</dt>
                                            <dd>143,000円</dd>
                                            <dt>その他</dt>
                                            <dd>88,000円</dd>
                                        </dl></td>
                                </tr>
                                <tr>
                                    <th>在留資格取得許可申請</th>
                                    <td>77,000円</td>
                                </tr>
                                <tr>
                                    <th>永住許可申請</th>
                                    <td>143,000円</td>
                                </tr>
                                <tr>
                                    <th>帰化許可申請</th>
                                    <td>165,000円</td>
                                </tr>
                                <tr>
                                    <th>就労資格証明書交付申請</th>
                                    <td>55,000円</td>
                                </tr>
                                <tr>
                                    <th>資格外活動許可申請</th>
                                    <td>27,500円</td>
                                </tr>
                                <tr>
                                    <th>再入国許可申請</th>
                                    <td>22,000円</td>
                                </tr>
                            </table>
                            <!--<div class="btn"><a href="#">入管・国際法務関係のサービス内容を見る</a></div>-->
                        </div>
                    </div>
                </div>
            </section>
            <section id="anc05">
                <div class="l-container">
                    <div class="outer">
                        <div class="inner">
                            <p class="p-service__ttl u-c-darkgreen">遺言・相続</p>
                            <table class="anc05">
                                <tr>
                                    <th>相続人及び相続財産の調査</th>
                                    <td>66,000円</td>
                                </tr>
                                <tr>
                                    <th>遺言書の起案及び作成指導</th>
                                    <td>66,000円</td>
                                </tr>
                                <tr>
                                    <th>遺産分割協議書の作成</th>
                                    <td>66,000円</td>
                                </tr>
                                <tr>
                                    <th>相続分なきことの証明書作成</th>
                                    <td>22,000円</td>
                                </tr>
                            </table>
                            <!--<div class="btn"><a href="#">遺言・相続のサービス内容を見る</a></div>-->
                        </div>
                    </div>
                </div>
            </section>
            <section id="anc05">
                <div class="l-container">
                    <div class="outer">
                        <div class="inner">
                            <p class="p-service__ttl u-c-darkgreen">書類作成・その他</p>
                            <table class="anc05">
                                <tr>
                                    <th>書類作成</th>
                                    <td><dl class="c-flex--between">
                                            <dt>内容証明郵便作成</dt>
                                            <dd>22,000円～</dd>
                                            <dt>契約書作成</dt>
                                            <dd>33,000円～</dd>
                                        </dl></td>
                                </tr>
                                <tr>
                                    <th>その他</th>
                                    <td><dl class="c-flex--between">
                                            <dt>日当</dt>
                                            <dd>22,000円</dd>
                                            <dt>顧問料</dt>
                                            <dd>33,000円</dd>
                                        </dl></td>
                                </tr>
                            </table>
                            <p>※ご依頼が確定しましたら、報酬額の50%＋実費を着手金として申し受けます。</p>
                            <p>※相談料　5,000円/1時間、訪問相談料　5,000円/1時間＋交通費実費</p>
                        </div>
                    </div>
                </div>
            </section>
            </div>
            <div class="l-container">
              <p class="other_service">その他の業務に関しても、お気軽にご相談ください。</p>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
