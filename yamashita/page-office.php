<?php
/*
Template Name: 事務所案内
*/
get_header();
?>
<div id="pageTitle" class="c-flex--col c-jus-center">
  <div class="l-container">
    <h1>事務所案内</h1>
  </div>
</div>
<div id="crumb">
  <ul class="l-container">
    <li><a href="../">トップページ</a></li>
    <li>事務所案内</li>
  </ul>
</div>
<div class="l-content">
  <div id="office">
    <section id="plofile">
      <div class="l-container">
        <div class="sec_ttl">
          <h2>代表プロフィール</h2>
        </div>
        <div class="inner c-flex--between c-flex--alignCenter c-flex--reverse">
          <div class="photo">
            <figure><img src="<?php echo get_template_directory_uri(); ?>/images/office/photo_yamashita.png" alt="" /></figure>
          </div>
          <div class="txt">
            <div class="block">
              <div class="block_ttl">
                <h3>「人財」が集まる強い組織づくりに、人に関する専門家として、<br class="br-pc">企業経営をサポートします。</h3>
              </div>
              <div class="comment">
                <p>経営上必要とされる4大要素「お金、モノ、人、情報」、中でも一番重要なのが「人」です。「人」がいなければ組織は成り立ちません。そのため、人を大切にし、一人ひとりが創造的に働ける環境を整えることは、効率化だけでなく持続可能な発展につながります。人に関する専門家として、事業の健全な発展に貢献できるようお手伝いをさせていただけましたら幸いです。</p>
              </div>
            </div>
            <div class="block">
              <div class="block_ttl">
                <h3>依頼者の心に寄り添い、<br class="br-pc">
                  行政手続きの専門家として地域社会に貢献します。</h3>
              </div>
              <div class="comment">
                <p>行政書士は官公署に提出する書類、権利義務・事実証明に関する書類の作成・提出代理を行う専門家です。頼れる街の法律家として、書類作成のプロとして、ご相談いただいた一つ一つの事件に丁寧に向き合い、お客様が安心して事業に専念していただけるよう精一杯のお手伝いをさせていただきたいと思います。</p>
              </div>
            </div>
            <div class="block">
              <div class="block_ttl">
                <h3>プロフィール</h3>
              </div>
              <div class="comment">
                <p>国立大学法人東京農工大学卒業。大学では農学部環境・資源学科で自然保護及び環境問題について学ぶ。卒業後は少林寺拳法の本山職員として勤務し、20年間、会報誌及び広報誌の創刊・制作に携わる。2018年1月、行政書士試験合格。同年5月、南青山に行政書事務所開業。2020年11月、社会保険労務士試験合格。2021年9月1日、事務所名をベリテ社労士・行政書士事務所に変更する。</p>
                <ul class="u-mt-20">
                  <li>社会保険労務士登録番号　第13210360号</li>
                  <li>東京都社会保険労務士会所属</li>
                  <li>行政書士登録番号　第18080873号</li>
                  <li>東京都行政書士会所属</li>
                  <li>申請取次行政書士</li>
                  <li>不当要求防止責任者講習受講済</li>
                  <li>著作権相談員</li>
                  <li>日商簿記2級</li>
                  <li>知的財産管理技能士3級</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="gaiyou">
      <div class="l-container">
        <div class="sec_ttl">
          <h2>事務所概要</h2>
        </div>
        <table>
          <tr>
            <th>事務所名</th>
            <td>ベリテ社労士・行政書士事務所</td>
          </tr>
          <tr>
            <th>所在地</th>
            <td>〒107－0062 東京都港区南青山4-20-20　<br class="br-sp">マックス南青山B1</td>
          </tr>
          <tr>
            <th>TEL</th>
            <td><span class="js-tel">090-9301-3749</span></td>
          </tr>
          <tr>
            <th>mail</th>
            <td>officeyamashita@sk-omotesando.com</td>
          </tr>
          <tr>
            <th>受付時間</th>
            <td>平日　10:00－18:00<br>
              ※ お電話またはメールにてご相談の予約をお受けいたします。時間外および土日でのご相談もお気軽にお尋ねください。</td>
          </tr>
          <tr>
            <th>休業日</th>
            <td>土日祝</td>
          </tr>
          <tr>
            <th>代表者</th>
            <td>山下真由美</td>
          </tr>
          <tr>
            <th>所属団体</th>
            <td>東京都社会保険労務士会<br>
              東京都行政書士会</td>
          </tr>
          <tr>
            <th>主な業務内容</th>
            <td class="srp">労働社会保険手続き、労務相談<br>労働者派遣事業の手続き<br>
              補助金・助成金の申請<br>
              著作権登録申請、裁定申請等<br>
              外国人の在留資格、永住許可申請等<br>
              遺言書、遺言分割協議書等の作成
              <div class="srp_wrap">
                    <div class="srp_box">
                      <div class="srp_box_img">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/srp.png" alt="SRPⅡ認証事務所">
                      </div>
                      <div class="srp_box_txt">
                        <p>ベリテ社労士事務所はSRPⅡ認証事務所です。<br>特定個人情報の適正な取扱いを実践してまいります。</p>
                        <p><a href="https://www.shakaihokenroumushi.jp/organization/tabid/507/Default.aspx" target="_blank" rel="noopener noreferrer">SRPⅡ認証制度とは</a></p>
                      </div>
                    </div>
                  </div>
            </td>
          </tr>
        </table>
      </div>
    </section>
    <section id="map">
      <div class="l-container">
        <div class="sec_ttl">
          <h2>アクセスマップ</h2>
        </div>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1254.1037261933336!2d139.71499169672302!3d35.66496260323275!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188b61eea8e261%3A0x40ae476535ad1a8!2z44CSMTA3LTAwNjIg5p2x5Lqs6YO95riv5Yy65Y2X6Z2S5bGx77yU5LiB55uu77yS77yQ4oiS77yS77yQ!5e0!3m2!1sja!2sjp!4v1548756027700" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </section>
  </div>
</div>
<?php get_footer(); ?>
