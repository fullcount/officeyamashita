<?php
/*
Template Name: トップページ
*/
get_header();
?>
<div class="top-mv">
  <div class="top-mv__image">
    <div class="top-mv__image__catch"><img src="<?php echo get_template_directory_uri(); ?>/images/top/mv.png" alt="「人材」ではなく「人財」へ、労務面から強い組織づくりをサポートします">
    </div>
  </div>
  <div class="top-mv__description">
    <div class="l-container">
      <h1 class="c-text">労働社会保険の手続きを中心に、人を大切に生き生きと働ける環境を整え「人財」が集まる強い組織づくりをサポートいたします。<br class="br-pc">また、官公署に提出する書類、権利義務・事実証明に関する書類の作成や相談といった行政書士業務についても、<br><strong>ベリテ社労士・行政書士事務所</strong>にお任せください。</h1>
    </div>
  </div>
</div>
<div id="toppage">
  <div class="p-service c-block">
    <h2 class="p-heading c-block__heading">業務事例</h2>
    <div class="p-service__block">
      <h3 class="p-service__sv-heading">社会保険労務士</h3>
      <div class="l-container">
        <div class="p-service__outer c-flex--between">
          <div class="p-service__inner">
            <img src="<?php echo get_template_directory_uri(); ?>/images/top/img_service01.png">
            <p>労働社会保険手続き、労務相談</p>
          </div>
          <div class="p-service__inner">
            <img src="<?php echo get_template_directory_uri(); ?>/images/top/img_service02.png">
            <p>労働者派遣事業の手続き</p>
          </div>
          <div class="p-service__inner">
            <img src="<?php echo get_template_directory_uri(); ?>/images/top/img_service03.png">
            <p>補助金・助成金の申請</p>
          </div>
        </div>
      </div>
    </div>
    <div class="p-service__block">
      <h3 class="p-service__sv-heading">行政書士</h3>
      <div class="l-container">
        <div class="p-service__outer c-flex--between">
          <div class="p-service__inner">
            <img src="<?php echo get_template_directory_uri(); ?>/images/top/img_service04.png">
            <p>著作権登録申請、裁定申請等</p>
          </div>
          <div class="p-service__inner">
            <img src="<?php echo get_template_directory_uri(); ?>/images/top/img_service05.png">
            <p>外国人の在留資格、永住許可申請等</p>
          </div>
          <div class="p-service__inner">
            <img src="<?php echo get_template_directory_uri(); ?>/images/top/img_service06.png">
            <p>遺言書、遺言分割協議書等の作成</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="p-about c-block c-block__baige">
    <h2 class="p-heading c-block__heading">ベリテ社労士・行政書士事務所について</h2>
    <div class="l-container c-flex--between">
      <div class="p-about__box">
        <a href="<?php echo home_url('/office/#profile'); ?>">
          <div class="p-about__photo"><img src="<?php echo get_template_directory_uri(); ?>/images/top/photo_profile.png" alt="行政書士プロフィール">
          </div>
          <p class="p-about__ttl">代表プロフィール</p>
        </a>
      </div>
      <div class="p-about__box">
        <a href="<?php echo home_url('/office/#gaiyou'); ?>">
          <div class="p-about__photo"><img src="<?php echo get_template_directory_uri(); ?>/images/top/photo_outline.png" alt="事務所概要">
          </div>
          <p class="p-about__ttl">事務所概要</p>
        </a>
      </div>
      <div class="p-about__box">
        <a href="<?php echo home_url('/office/#map'); ?>">
          <div class="p-about__photo"><img src="<?php echo get_template_directory_uri(); ?>/images/top/photo_access.png" alt="アクセスマップ">
          </div>
          <p class="p-about__ttl">アクセスマップ</p>
        </a>
      </div>
    </div>
  </div>
  <div class="p-news c-block">
    <h2 class="p-heading c-block__heading">お知らせ</h2>
    <div class="p-news__outer">
      <?php
    $args = array(
      'posts_per_page' => 5 // 表示件数の指定
    );
    $posts = get_posts( $args );
    foreach ( $posts as $post ): // ループの開始
      setup_postdata( $post ); // 記事データの取得
    ?>
      <dl class="p-news__block">
        <dt class="p-news__date">
          <?php the_time('Y/m/d'); ?>
        </dt>
        <dd class="p-news__ttl">
          <a href="<?php the_permalink(); ?>">
            <?php the_title(); ?>
          </a>
        </dd>
        <?php
  endforeach; // ループの終了
  wp_reset_postdata(); // 直前のクエリを復元する
?>
      </dl>
      <!--<div class="p-news__button"><a href="./news/" class="c-button c-button__df c-button__arrow c-button__arrow-right">詳しくはこちら</a></div>-->
    </div>
  </div>
</div>
<?php get_footer(); ?>
