<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="robots" content="all">
    <meta content="True" name="HandheldFriendly">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="">
    <meta name="Keywords" content="">
    <title>ベリテ社労士・行政書士事務所</title>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css" type="text/css" />
<?php wp_head(); ?>
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;700&display=swap" rel="stylesheet">
</head>

<body <?php body_class(); ?>>
    <div id="wrapper" class="l-wrapper">
        <header class="l-header">
            <div class="l-header__logo c-logo"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="ベリテ社労士・行政書士事務所"></a></div>
            <ul class="hnav">
                <li id="nav-toggle"> <span></span><span></span><span></span></li>
            </ul>
            <nav class="gnav">
                <ul class="c-flex--alignCenter c-jus-center">
                    <li class="gnav__item"><a href="<?php echo home_url(); ?>">HOME</a></li>
                    <li class="gnav__item"><a href="<?php echo home_url('/service'); ?>">業務内容</a></li>
                    <li class="gnav__item"><a href="<?php echo home_url('/office'); ?>">事務所案内</a></li>
                    <li class="gnav__item"><a href="<?php echo home_url('/price'); ?>">報酬案内</a></li>
                    <!--<li class="gnav__item"><a href="<?php echo home_url('/news'); ?>">お知らせ</a></li>-->
                    <li class="gnav__item"><a href="<?php echo home_url('/contact'); ?>">お問い合わせ</a></li>
                </ul>
            </nav>
        </header>
